package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"sync"

	"flag"

	"gitlab.com/marotpam/vat_checker/validation"
)

func validate(vatNumber string) {
	valid, err := validation.IsValid(vatNumber)
	if err != nil {
		log.Printf("Error validating %s: %v\n", vatNumber, err)
	}

	fmt.Printf("%s -> %v\n", vatNumber, valid)
}

func validateSerial(r *csv.Reader) {
	for row, err := r.Read(); err != io.EOF; row, err = r.Read() {
		validate(row[1])
	}
}

func validateWithWorker(vatNumber string, wg *sync.WaitGroup) {
	defer wg.Done()

	validate(vatNumber)
}

func validateConcurrently(r *csv.Reader) {
	var wg sync.WaitGroup
	for row, err := r.Read(); err != io.EOF; row, err = r.Read() {
		wg.Add(1)
		go validateWithWorker(row[1], &wg)
	}
	wg.Wait()
}

func main() {
	csvPath := flag.String("path", "", "File to read vat numbers from")
	mode := flag.String("mode", "serial", "Execution mode: serial or concurrent")
	flag.Parse()

	if *csvPath == "" {
		log.Fatalf("Need to provide a CSV file")
	}

	f, err := os.Open(*csvPath)

	if err != nil {
		log.Fatalf("Failed reading file %s: %v", *csvPath, err)
	}

	r := csv.NewReader(bufio.NewReader(f))
	switch *mode {
	case "serial":
		validateSerial(r)

	case "concurrent":
		validateConcurrently(r)

	default:
		log.Fatalf("%s is not a valid mode, only serial or concurrent are accepted", *mode)
	}
}
