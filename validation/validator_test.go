package validation

import "testing"

func TestIsValidWhenServiceIsRunning(t *testing.T) {
	type out struct {
		v   bool
		err error
	}
	type tc struct {
		name string
		in   string
		out  out
	}
	testCases := []tc{
		{
			name: "Valid VAT number",
			in:   "NL009447878B01",
			out: out{
				v:   true,
				err: nil,
			},
		}, {
			name: "Invalid VAT number",
			in:   "invalid_vat",
			out: out{
				v:   false,
				err: nil,
			},
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			valid, err := IsValid(tt.in)

			if err != tt.out.err {
				t.Errorf("Failed checking error %v matches expected %v", err, tt.out.err)
			}

			if valid != tt.out.v {
				t.Errorf("%s should be %v, but reported as %v", tt.in, tt.out.v, valid)
			}
		})
	}
}
