package validation

import (
	"github.com/dannyvankooten/vat"
)

func IsValid(vatNumber string) (bool, error) {
	return vat.ValidateNumber(vatNumber)
}
